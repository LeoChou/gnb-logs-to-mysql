import mysql.connector
import re
from collections import deque
import time

# Create a database connection
conn = mysql.connector.connect(
    host='10.16.8.4',  # MySQL database host address (optional: '192.168.31.13')
    user='oai-mysql',  # Your MySQL username
    password='welcome',  # Your MySQL password
    database='gNB_data'  # Your MySQL database name
)

cursor = conn.cursor()

# Create a table if it doesn't exist
cursor.execute('''
    CREATE TABLE IF NOT EXISTS communication_rates (
        id INT AUTO_INCREMENT PRIMARY KEY,
        registration_timestamp FLOAT,
        registration_date DATETIME DEFAULT CURRENT_TIMESTAMP,
        frame_slot FLOAT, rnti VARCHAR(255), ph INT, pcmax INT, average_rsrp INT,
        dlsch_rounds VARCHAR(255), dlsch_errors INT, pucch0_dtx INT, d_bler FLOAT, d_mcs INT,
        dlsch_total_bytes INT,
        ulsch_rounds VARCHAR(255), ulsch_dtx INT, ulsch_errors INT, u_bler FLOAT, u_mcs INT,
        ulsch_total_bytes_scheduled INT, ulsch_total_bytes_received INT,
        lcid1_bytes_tx INT,
        lcid4_bytes_tx INT,
        lcid4_bytes_rx INT,
        tx_rate FLOAT,
        rx_rate FLOAT,
        tx_rate_avg FLOAT,
        rx_rate_avg FLOAT
    )
''')

inti = True
pre_frame_slot = None  # Avoid duplicate imports due to synchronization issues
while True:
    # Open the text file and read the last 11 lines
    with open('output.log', 'r') as file:
        lines = deque(file, 11)

    # Use regular expressions to extract data
    pattern = r'Frame\.Slot (\d+\.\d+).*?' \
              r'RNTI (\w+) \(\d+\) PH (\d+) dB PCMAX (\d+) dBm, average RSRP (-\d+) .*?' \
              r'dlsch_rounds (\d+/\d+/\d+/\d+), dlsch_errors (\d+), pucch0_DTX (\d+), BLER (\d+\.\d+) MCS (\d+).*?' \
              r'dlsch_total_bytes (\d+).*?' \
              r'ulsch_rounds (\d+/\d+/\d+/\d+), ulsch_DTX (\d+), ulsch_errors (\d+), BLER (\d+\.\d+) MCS (\d+).*?' \
              r'ulsch_total_bytes_scheduled (\d+), ulsch_total_bytes_received (\d+).*?' \
              r'LCID 1: (\d+) bytes TX.*?' \
              r'LCID 4: (\d+) bytes TX.*?' \
              r'LCID 4: (\d+) bytes RX'

    matches = re.findall(pattern, '\n'.join(lines), re.DOTALL)
    if matches != []:

        # Insert data into the database
        [(frame_slot, rnti, ph, pcmax, average_rsrp, dlsch_rounds, dlsch_errors, pucch0_dtx, d_bler, d_mcs,
          dlsch_total_bytes, ulsch_rounds, ulsch_dtx, ulsch_errors, u_bler, u_mcs, ulsch_total_bytes_scheduled,
          ulsch_total_bytes_received, lcid1_bytes_tx, lcid4_bytes_tx, lcid4_bytes_rx)] = matches

        if pre_frame_slot != frame_slot:

            # Check if it's the first time inserting data
            if inti:
                prev_lcid4_bytes_tx = lcid4_bytes_tx
                prev_lcid4_bytes_rx = lcid4_bytes_rx
                inti = False
                pre_time = time.time()
                registration_timestamp = 1

            # Calculate the packet transmission rate difference
            tx_rate = (int(lcid4_bytes_tx) - int(prev_lcid4_bytes_tx)) / registration_timestamp
            rx_rate = (int(lcid4_bytes_rx) - int(prev_lcid4_bytes_rx)) / registration_timestamp
            if tx_rate < 0:
                tx_rate = 0
            if rx_rate < 0:
                rx_rate = 0
            prev_lcid4_bytes_tx = lcid4_bytes_tx
            prev_lcid4_bytes_rx = lcid4_bytes_rx

            # Execute an SQL query to retrieve the last three rows of the tx_rate column, but not exceeding the actual number of rows
            query = "SELECT tx_rate FROM communication_rates ORDER BY id DESC LIMIT 3"
            cursor.execute(query)

            # Retrieve the query results
            last_three_tx_rates = cursor.fetchall()
            last_three_tx_rates = [tx_rate_indx[0] for tx_rate_indx in last_three_tx_rates]
            last_three_tx_rates.append(tx_rate)

            # Execute an SQL query to retrieve the last three rows of the rx_rate column, but not exceeding the actual number of rows
            query = "SELECT rx_rate FROM communication_rates ORDER BY id DESC LIMIT 3"
            cursor.execute(query)

            # Retrieve the query results
            last_three_rx_rates = cursor.fetchall()
            last_three_rx_rates = [rx_rate_indx[0] for rx_rate_indx in last_three_rx_rates]
            last_three_rx_rates.append(rx_rate)

            # Calculate the averages
            tx_rate_avg = sum(last_three_tx_rates)/len(last_three_tx_rates)
            rx_rate_avg = sum(last_three_rx_rates)/len(last_three_rx_rates)

            cursor.execute('''
                INSERT INTO communication_rates (
                    frame_slot, registration_timestamp, rnti, ph, pcmax, average_rsrp,
                    dlsch_rounds, dlsch_errors, pucch0_dtx, d_bler, d_mcs,
                    dlsch_total_bytes,
                    ulsch_rounds, ulsch_dtx, ulsch_errors, u_bler, u_mcs,
                    ulsch_total_bytes_scheduled, ulsch_total_bytes_received,
                    lcid1_bytes_tx,
                    lcid4_bytes_tx,
                    lcid4_bytes_rx,
                    tx_rate,
                    rx_rate,
                    tx_rate_avg,
                    rx_rate_avg
                )
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                %s, %s)
            ''', (
                frame_slot, registration_timestamp, rnti, ph, pcmax, average_rsrp, dlsch_rounds, dlsch_errors,
                pucch0_dtx,
                d_bler, d_mcs, dlsch_total_bytes, ulsch_rounds, ulsch_dtx, ulsch_errors, u_bler, u_mcs,
                ulsch_total_bytes_scheduled, ulsch_total_bytes_received, lcid1_bytes_tx, lcid4_bytes_tx,
                lcid4_bytes_rx,
                tx_rate, rx_rate, tx_rate_avg, rx_rate_avg))

            # Commit the changes
            conn.commit()

            # Query the current row count
            cursor.execute("SELECT COUNT(*) FROM communication_rates")
            row_count = cursor.fetchone()[0]

            # If the row count exceeds 60, delete the oldest row
            if row_count > 60:
                cursor.execute("DELETE FROM communication_rates ORDER BY id LIMIT 1")
                conn.commit()
            pre_frame_slot = frame_slot

            # Query the contents of the table named 'communication_rates'
            cursor.execute('SELECT * FROM communication_rates')

            # Retrieve all rows
            rows = cursor.fetchall()

            # Print the contents of the table
            print(rows[-1])

        conn.close()