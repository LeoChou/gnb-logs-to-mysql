import re
import time

output_filename = "output-log_temp3.25.1_00101_test.log"
output_realtime_filename = "output.log"

with open('output.log', 'w') as file:
    # Do not write anything, leave the file empty
    pass

# Read the contents of output_filename
with open(output_filename, "r") as infile:
    lines = infile.readlines()

while True:
    lines_to_append = []

    for line in lines:
        if "Frame.Slot" in line:
            if lines_to_append:
                break
        lines_to_append.append(line)

    if lines_to_append:
        with open(output_realtime_filename, "a") as outfile:
            outfile.write("".join(lines_to_append))
        print(f"Appended {len(lines_to_append)} lines to {output_realtime_filename}")
        print(lines_to_append)

    # Remove the inserted lines
    lines = lines[len(lines_to_append):]

    # Loop through the content again if it's less than 15 lines
    if len(lines) < 15:
        # Read the contents of output_filename
        with open(output_filename, "r") as infile:
            lines = infile.readlines()

    time.sleep(1)
