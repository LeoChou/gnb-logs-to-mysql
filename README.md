## OAI平台可视化-数据提取脚本
### 介绍
将OAI运行系统中的基站状态实时提取为数据保存到数据库'gNB_data'中。 方便后续的数据库操作。

### 运行
#### 本地测试运行：
IDE运行`realtime.py`(模拟OAI基站状态输出的脚本) 和 `pattern.py`(数据库写入脚本)
```puml
python3 realtime.py
```
```puml
python3 pattern.py
```

#### 正式运行
前提: OAI模拟程序正常运行中,登录OAI系统在项目路径中`openairinterface5g/cmake_targets/ran_build/build`执行`pattern.py`

在Windows上，你可以使用命令提示符或PowerShell来执行SCP操作。(也可通过其他手段传输文件并运行指令，如ssh协议)

在项目文档打开终端，拷贝文件到远端：
```puml
scp -P 60006 .\pattern.py oai@10.16.8.4:/home/oai/openairinterface5g-devel/openairinterface5g/cmake_targets/ran_build/build 
```
运行：
```puml
scp -P 60006 oai@10.16.8.4 "python3 /home/oai/openairinterface5g-devel/openairinterface5g/cmake_targets/ran_build/build/pattern.py"
```

### 补充
数据库地址：莞工校园网内通过下面的地址连接![img.png](image/img.png)
